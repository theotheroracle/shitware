prefix?=/usr
INSTALL?=install
RM=rm -rf
BUILD_DIR?=build

$(BUILD_DIR)/shitware $(BUILD_DIR)/shitware.sh.x.c: shitware.sh
	mkdir -p $(BUILD_DIR)
	shc -Uvrf shitware.sh -o $(BUILD_DIR)/shitware
	mv shitware.sh.x.c $(BUILD_DIR)/shitware.sh.x.c
install: $(BUILD_DIR)/shitware
	$(INSTALL) -d $(DESTDIR)$(prefix)/bin
	$(INSTALL) -m 755 $(BUILD_DIR)/shitware $(DESTDIR)$(prefix)/bin
uninstall:
	$(RM) $(DESTDIR)$(prefix)/bin/shitware
clean: 
	$(RM) $(BUILD_DIR)
